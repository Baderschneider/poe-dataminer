from src.config import database_config
from src.data_processor.item_processor import ItemProcessor
from src.data_types.account import Account
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from src.data_types.base import Base
from src.data_types.item import Item
from src.requestManager import RequestManager
import sys
import time


def main(Session, next_change_id_sp="EMPTY"):
    session = Session()

    all_accounts = session.query(Account).all()
    all_items = session.query(Item).all()

    ItemProcessor.generate_modifier_list(all_items, session)

    all_stashes_number = 0
    request_manager = RequestManager(session, next_change_id_sp)


    while True:
        start_time = time.time()
        new_account = []
        workable_stashes = []
        resp = request_manager.next_request()
        print("--- request took seconds --- " + str((time.time() - start_time)))
        start_time = time.time()
        all_stashes = resp["stashes"]
        all_stashes_number = all_stashes_number + len(all_stashes)

        for stash in all_stashes:
            if (stash["stashType"] == "PremiumStash") and (stash["public"]):
                workable_stashes.append(stash)

        print("--- finding all premium stashes took seconds --- " + str((time.time() - start_time)))
        start_time = time.time()

        for stash in workable_stashes:
            account = account_exists(all_accounts, stash["accountName"])
            if account is None:
                account = Account(stash["accountName"])
                all_accounts.append(account)
                new_account.append(account)
            account.process_stash(stash, session)

        print("--- processing all stashes took seconds --- " + str((time.time() - start_time)))
        start_time = time.time()

        for acc in new_account:
            session.add(acc)
        session.commit()
        print_update(all_accounts, all_stashes_number, request_manager.url + request_manager.msg.next_update_message)


def account_exists(all_accounts, name):
    for account in all_accounts:
        if account.name == name:
            return account
    return None


def print_update(all_accounts, all_stashes_number, url):
    print("")
    print("Processed " + str(all_stashes_number) + " Stash Tabs in total")
    print("Using page : " + url)
    print("Processed " + str(len(all_accounts)) + " Accounts until now")


if __name__ == "__main__":

    engine = create_engine(database_config.db_address, echo=database_config.database_logging)
    Session = sessionmaker(bind=engine)
    Base.metadata.create_all(engine)
    if len(sys.argv) > 1:
        next_change_id_sp = sys.argv[1]
        main(Session, next_change_id_sp)
    else:
        main(Session)
