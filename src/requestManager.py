import requests
from src.data_types.update_message import UpdateMessage


class RequestManager():

    def __init__(self, session, next_change_id_sp="EMPTY"):
        self.headers2 = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1'
        }
        update_message = session.query(UpdateMessage).all()

        if next_change_id_sp != "EMPTY":
            self.msg = UpdateMessage(next_change_id_sp)
        else:
            if len(update_message) == 0:
                self.msg = UpdateMessage("EMPTY")
            else:
                self.msg = update_message[0]

        self.base_url = "http://www.pathofexile.com/api/public-stash-tabs"
        self.url = "http://www.pathofexile.com/api/public-stash-tabs?id="
        self.session = session

    def set_change_id(self, new_change_id):
        self.next_change_id = new_change_id

    def next_request(self):
        if self.msg.next_update_message == "EMPTY":
            response = requests.get(self.base_url, headers=self.headers2)
            response = response.json()
            self.msg.update_update_message(response["next_change_id"])
            self.session.add(self.msg)

        else:
            response = requests.get(self.url + self.msg.next_update_message, headers=self.headers2)
            response = response.json()
            self.msg.update_update_message(response["next_change_id"])
            self.session.add(self.msg)
        return response
