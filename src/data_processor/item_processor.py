import ast
from string import digits

from src.data_types.modifiers import Modifier


class ItemProcessor:

    @staticmethod
    def generate_modifier_list(items,session):
        all_explicits = []
        all_impilicts = []
        all_sockets = []
        print(len(items))
        for item in items:
            if hasattr(item, 'explicitMods') and item.explicitMods is not None:
                item_explicit_mods = ast.literal_eval(item.explicitMods)
                for value in item_explicit_mods:

                    remove_digits = str.maketrans('', '', digits)
                    explicit_modifier = value.translate(remove_digits)

                    if not explicit_modifier in all_explicits:
                        all_explicits.append(explicit_modifier)
                        session.add(Modifier(explicit_modifier, "Explicit Modifier"))

            if hasattr(item, 'implicitMods') and item.implicitMods is not None:
                item_implicit_mods = ast.literal_eval(item.implicitMods)
                for value in item_implicit_mods:

                    remove_digits = str.maketrans('', '', digits)
                    implicit_modifier = value.translate(remove_digits)

                    if not implicit_modifier in all_impilicts:
                        all_impilicts.append(implicit_modifier)
                        session.add(Modifier(implicit_modifier, "Implicit Modifier"))

            if hasattr(item, 'sockets') and item.sockets is not None:
                item_sockets = ast.literal_eval(item.sockets)
                for value in item_sockets:
                    if not value in all_sockets:
                        all_sockets.append(value)
                        session.add(Modifier(str(value), "Socket Modifier"))


        print(len(all_sockets))
        print(len(all_impilicts))
        print(len(all_explicits))