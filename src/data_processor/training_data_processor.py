import ast
from string import digits

from src.data_types.vanish_event import VanishEvent


class TrainingDataProcessor:

    @staticmethod
    def generate_training_data(vanish_events,session):
        return_list = []
        all_modifier = session.query(VanishEvent).all()


        for event in vanish_events:
            modifier = ast.literal_eval(item.explicitMods)
            for item in modifier
            remove_digits = str.maketrans('', '', digits)

            explicit_modifier = event..translate(remove_digits)
