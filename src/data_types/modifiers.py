from src.data_types.mixin.mixin_modifier import MixinModifier
from src.data_types.base import Base

class Modifier(Base, MixinModifier):
    def __init__(self, modifier,type):
        self.modifier = modifier
        self.type = type