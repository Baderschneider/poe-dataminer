from datetime import datetime
from src.data_types.base import Base
from src.data_types.mixin.mixin_vanish_event import MixinVanishEvent


class VanishEvent(Base, MixinVanishEvent):

    def __init__(self, item):
        self.name = item.name
        self.i_id = item.i_id
        self.type_line = item.type_line
        self.note = item.note
        self.item_level = item.item_level
        self.creation_date = item.creation_date

        if hasattr(item, 'properties'):
            self.properties = item.properties
        if hasattr(item, 'requirements'):
            self.requirements = item.requirements
        if hasattr(item, 'explicitMods'):
            self.explicitMods = item.explicitMods
        if hasattr(item, 'implicitMods'):
            self.implicitMods = item.implicitMods
        if hasattr(item, 'sockets'):
            self.sockets = item.sockets
        if hasattr(item, 'corrupted'):
            self.corrupted = item.corrupted
        now = datetime.now()
        self.vanish_date = now.strftime("%d/%m/%Y %H:%M:%S")
