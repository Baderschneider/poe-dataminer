from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from src.data_types.base import Base
from src.data_types.item import Item
from src.data_types.mixin.mixin_stash import MixinStash


class Stash(Base, MixinStash):
    account_id = Column(Integer, ForeignKey('account.id'))
    account = relationship("Account", back_populates="stashes")

    items_for_sell = relationship("Item", back_populates="stash")

    def __init__(self, id,stash_type,league,items):
        self.id = id
        self.stash_type = stash_type
        self.league = league
        self.items_for_sell = []

        for item in items:
            if "note" in item:
                new_item = Item.item_from_req(item)
                self.items_for_sell.append(new_item)

    @staticmethod
    def stash_from_req(req):
        id = req["id"]
        stash_type = req["stashType"]
        league = req["league"]
        items = req["items"]

        return Stash(id,stash_type,league,items)