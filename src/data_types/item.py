from sqlalchemy.orm import relationship
from src.data_types.base import Base
from sqlalchemy import Column, Integer, ForeignKey
from datetime import datetime
from src.data_types.mixin.mixin_item import MixinItem


class Item(Base, MixinItem):
    stash_id = Column(Integer, ForeignKey('stash.id'))
    stash = relationship("Stash", back_populates="items_for_sell")

    def __init__(self, name,i_id,type_line,note,item_level):
        self.name = name
        self.i_id = i_id
        self.type_line = type_line
        self.note = note
        self.item_level = item_level
        now = datetime.now()
        self.creation_date = now.strftime("%d/%m/%Y %H:%M:%S")

    @staticmethod
    def item_from_req(req):
        item_name = req["name"]
        i_id = req["id"]
        type_line = req["typeLine"]
        note = req["note"]
        item_level = req["ilvl"]

        item = Item(item_name,i_id,type_line,note,item_level)

        if "properties" in req:
            item.properties = str(req["properties"])
        if "requirements" in  req:
            item.requirements = str(req["requirements"])
        if "explicitMods" in req:
            item.explicitMods = str(req["explicitMods"])
        if "implicitMods" in req:
            item.implicitMods = str(req["implicitMods"])
        if "sockets" in req:
            item.sockets = str(req["sockets"])
        if "corrupted" in req:
            item.corrupted = req["corrupted"]

        return item
