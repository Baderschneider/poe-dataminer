from sqlalchemy import Column, String, Integer, Boolean


class MixinVanishEvent:
    __tablename__ = 'vanish_event'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    i_id = Column(String)
    type_line = Column(String)
    base_type = Column(String)
    item_level = Column(Integer)
    note = Column(String)
    properties = Column(String)
    requirements = Column(String)
    explicitMods = Column(String)
    implicitMods = Column(String)
    sockets = Column(String)
    corrupted = Column(Boolean)
    creation_date = Column(String)
    vanish_date = Column(String)
