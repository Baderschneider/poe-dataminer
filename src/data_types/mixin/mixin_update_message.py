from sqlalchemy import Column, String, Integer


class MixinUpdateMessage:
    __tablename__ = 'update_message'
    id = Column(Integer, primary_key=True)
    next_update_message = Column(String)
