from sqlalchemy import Column, String, Integer


class MixinAccount:
    __tablename__ = 'account'
    id = Column(Integer, primary_key=True)
    name = Column(String)