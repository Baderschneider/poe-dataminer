from sqlalchemy import Column, String, Integer


class MixinModifier:
    __tablename__ = 'modifier'
    id = Column(Integer, primary_key=True)
    modifier = Column(String)
    type = Column(String)