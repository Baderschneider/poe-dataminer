from sqlalchemy import Column, String


class MixinStash:
    __tablename__ = 'stash'
    id = Column(String, primary_key=True)
    stash_type = Column(String)
    league = Column(String)
