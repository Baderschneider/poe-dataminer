from src.data_types.base import Base

from src.data_types.mixin.mixin_update_message import MixinUpdateMessage


class UpdateMessage(Base, MixinUpdateMessage):

    def __init__(self, next_update_message):
        self.next_update_message = next_update_message

    def update_update_message(self, msg):
        self.next_update_message = msg
