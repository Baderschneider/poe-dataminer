from sqlalchemy.orm import relationship

from src.data_types.base import Base
from src.data_types.mixin.mixin_account import MixinAccount
from src.data_types.stash import Stash
from src.data_types.vanish_event import VanishEvent


class Account(Base, MixinAccount):
    stashes = relationship("Stash", back_populates="account")

    def __init__(self, name):
        self.name = name
        self.items_for_sell = []

    def check_if_stash_exists(self, check_stash_id):
        for stash in self.stashes:
            if stash.id == check_stash_id:
                return stash
        return None

    def process_stash(self, req_msg, session):
        stash = self.check_if_stash_exists(req_msg["id"])
        if stash is None:
            stash = Stash.stash_from_req(req_msg)
            self.stashes.append(stash)
            session.add(stash)
        else:
            new_stash = Stash.stash_from_req(req_msg)
            self.process_double(stash, new_stash, session)
            self.stashes.remove(stash)
            session.delete(stash)
            stash = Stash.stash_from_req(req_msg)
            self.stashes.append(stash)
            session.add(stash)

    def process_double(self, stash, new_stash, session):
        for item in stash.items_for_sell:
            removed = True
            for new_item in new_stash.items_for_sell:
                if item.i_id == new_item.i_id:
                    removed = False
            if removed:
                vanish_event = VanishEvent(item)
                session.add(vanish_event)
