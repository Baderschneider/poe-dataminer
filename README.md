A Dataminer for the Public stash tab API from Grinding Gear Games game Path of Exile.
Meant to extract information about sell offers for Ingame items by comparing different versions of stashes and Pricing and extracting information about actual sells

Uses Python reuests to request data from the API, converts it to JSON, filters nonessential information by comparing sell offer versions at different moment in time and saves information in a local database using SQLAlchemys ORM functionality

Processes Saved data to extract all Existing pre- und suffix Modifiers.

Saves all found item Modifiers in order to build modifier vectors as preperation for ML training Data

Can be started with next_change_id command line parameter to start reading the public stash API at the newest state instead of loading heaps of unnecessary historical data.
